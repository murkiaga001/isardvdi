export const sectionConfig = {
  desktops: {
    statusBar: 'StatusBar'
  },
  desktopsnew: {
    statusBar: 'StatusBar'
  },
  deployment: {
    statusBar: 'StatusBar'
  },
  deployment_videowall: {
    statusBar: 'DeploymentVideowallStatusBar'
  },
  deployments: {
    statusBar: 'StatusBar'
  },
  images: {
    statusBar: 'ImagesStatusBar'
  },
  booking: {
    statusBar: 'BookingStatusBar'
  },
  default: {
    statusBar: 'StatusBar'
  }
}
